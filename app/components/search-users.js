import React, { Component } from 'react';
import $ from 'jquery';
import {BarChart} from 'react-easy-chart';
import {Legend} from 'react-easy-chart';

export default class SearchUsers extends Component{
    constructor() {
	    super();

        this.state = {username: '', list: [], repo: [], total_users: '', total_contributions: ''};
        this.setUsername = this.setUsername.bind(this);
        this.setUserRepo = this.setUserRepo.bind(this);
        this.getData = this.getData.bind(this);
        this.getRepoStats = this.getRepoStats.bind(this);
    }

    setUsername(data){
        this.setState({username: data.target.value});
    }
    
    setUserRepo(data){
        this.setState({repo: data.target.value});
    }

    getRepoStats(data){
        $.ajax({
            url: data.target.value + '/contributors',
            dataType: 'json',
            success: function(data){
                var total_contributions = 0;

                if(data.length){
                    for(var i = 0; i < data.length; i++){
                        total_contributions = total_contributions + data[i].contributions
                        
                    }

                    this.setState({total_users: data});
                    this.setState({total_contributions: total_contributions});
                }

            }.bind(this), 
            error: function(data){
                if(data.status === 404){
                    alert(data.statusText);
                }
            }
        });
    }

    getData(e){
        e.preventDefault();

        $.ajax({
			url: 'https://api.github.com/users/'+ this.state.username +'/repos',
			dataType: 'json',
			success: function(data){
                this.setState({list: data});

                alert('Take a look at the dropdown!');

			}.bind(this), 
			error: function(data){
				if(data.status === 404){
					console.log(data.statusText);
				}
			}
		});
    }
    
    render(){
        return(
            <div>
                <h1 className="title">Search users or repositories</h1>
            
                <form onSubmit={this.getData}>
                    <input type="text" name="username" className="" onChange={this.setUsername} label="username" />
                </form>

                <form>
                    <select onChange={this.getRepoStats}>
                        <option>Take a look if have any repo</option>
                        {
                            this.state.list.map(function(data){
                                return(
                                    <option value={data.url} key={data.id}> {data.name } </option>   
                                )
                            })
                        }
                    </select>
                </form>
                
                <div className="charts">
                    <BarChart colorBars height={150} width={500} axes
                        data={[
                            {
                                x: 'Users',
                                y: this.state.total_users.length,
                                color: '#999'
                            },
                            {
                                x: 'Contributions',
                                y: this.state.total_contributions,
                                color: '#998'
                            }
                        ]}
                    />
                </div>
            </div>
        );
    }
}